const Starter = require('./Starter');

function thisFunctionKnowsNothing() {
    const starter = new Starter();
    starter.begin();
    /*
        Do some work here and then function ends.
        As the function ends, "starter" will also be removed by garbage collector,
        as there is no one else referencing "starter".
    */
}

console.log('START');

thisFunctionKnowsNothing();

console.log('END');
