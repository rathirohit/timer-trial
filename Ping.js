class Ping {
  constructor(interval) {
    this.interval = interval;
  }

  start(X) {
    this.stop();
    this.timerID = setInterval(() => {
      console.log('===> ping <===');
      console.log(X);
    }, this.interval);
  }

  stop() {
    clearInterval(this.timerID);
    this.timerID = null;
  }
}

module.exports = Ping;
